# MSTVServerSelector

## What is it?

MSTVServerSelector is a ViewController that presents a list of environments for your apps.
This is meant to facilitate testing the same app against different environments with each their own configurations.

This UI is meant to be shown at startup (hooks in application:didFinishLaunchingWithOptions:) when run on a DEV target
and be COMPLETELY absent on production builds.

Please note that it is **VERY IMPORTANT** that this code is not linked against a production build and no resource files associated with it are.
Because this could lead to sensitive informations leak. (staging servers url + username/password maybe)


## How to use it?

### Step 1: add to cocoapods

Simple enough, you only need to put the following code in your Podfile.
The key here, _(again)_ is to link the pod only against your DEV target.

First of all, add this repository as a source:

    source 'https://bitbucket.org/rbardou/mstv-server-selector.git'

Then add the pod only to your dev target:

    target :myapptarget_dev do
        pod 'MSTVServerSelector'
    end


Then, as usual, run

    pod install


### Step 2: Edit your AppDelegate code

As mentioned before, we advise against embedding the server-selector code in a production build.

This is why the best course of action to load the server-selector on startup is to check for the availability of the MSTVServerSelectorViewController class and then make it
the first UIWindow's rootViewController.

Then setup a block to be called when the user chooses the environment to load. In this block, replace the rootViewController with the one you initially wanted to load.

Check-out the demo project "AppDelegate" file for a nice example.

Example:

	- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
	{
		// Window
	    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	    self.window.backgroundColor = [UIColor clearColor];
    
	    void(^appLoadingComplete)() = ^{
        
	        MyRootViewController *rootViewController = [[MyRootViewController alloc]
	                                                             init];
        
	        self.window.rootViewController = rootViewController;
	        [self.window makeKeyAndVisible];
	    };
    
	    [self loadServerSelectorIfNecessaryThenProceedWith:appLoadingComplete];
    
	    return YES;
	}

	- (void) loadServerSelectorIfNecessaryThenProceedWith:(void (^)())proceedBlock {
	#ifndef IS_PROD
	    BOOL loadServerSelector = NO;
	    Class serverSelectorVCClass = NSClassFromString(@"MSTVServerSelectorViewController");
	    if (serverSelectorVCClass) {
	        loadServerSelector = YES;
	    }
    
	    if (loadServerSelector) {
	        MSTVServerSelectorViewController *serverSelectorVC = [[serverSelectorVCClass alloc] init];
	        serverSelectorVC.onServerSelected = ^(NSString *envName, NSDictionary *payload, BOOL environmentHasChangedSinceLastLaunch) {
	            myAppConfig *config = [[myAppConfig alloc] init];
	            [payload enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
	                [config setValue:obj forKey:key];
	            }];
	            self.activeConfig = config;
            
	            if (environmentHasChangedSinceLastLaunch) {
	                // Clean stuff
	                _LOG(@"SELECTED ENVIRONMENT CHANGED SINCE LAST TIME");
	                _LOG(@"CLEARING DOCUMENTS DIRECTORY");
                
	                NSString *folderPath = [UIApplication documentDirectoryPath];
	                NSError *error = nil;
                
	                NSArray *documentsDirContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error];
	                for (NSString *file in documentsDirContent) {
	                    [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
	                    if (error) {
	                        _LOG(@"ERROR deleting %@ (%@)", file, error);
	                    }
	                }
	            }
            
	            proceedBlock();
	        };
        
	        self.window.rootViewController = serverSelectorVC;
	        [self.window makeKeyAndVisible];
    	} else {
		#endif
        	proceedBlock();
		#ifndef IS_PROD
    	}
	#endif

	}

### Step 3: Add the configuration file

#### Add / Create "server-selector-config.plist"
You should create and add to your Xcode project a file named "server-selector-config.plist"

Here is a script that will create a vanilla file for you:
TODO: put code here

Or you can create it yourself by following this structure.

* **config** _[dict]_
	* **autoloading\_delay**
	* **color\_alternate**
	* **color\_cell**
	* **color\_cell\_separator**
	* **color\_header\_bg**
	* **color\_header\_separator**
	* **color\_main**
	* **host\_http\_header**
	* **host\_status\_code\_expected**
	* **host\_timeout**
* **environments** _[array]_
	* [0] _[dict]_
		* **display\_name** _[string]_		-> display name for your server
		* **display\_order** _[number]_ 	-> display order for your server (to force some servers on top)
		* **is\_prod** _[bool]_ 			-> set to true to mark server as prod (it will have a different look in the list)
		* **host\_url** _[string]_ 			-> your server url, this is only used to ping it
		* **payload** _[dict]_				-> this is the configuration dictionary passed to your block when starting the app -> anything you want

#### Make sure it's only associated with your dev target

You should NEVER let this file be part of your AppStore distribution bundle, as sensitive informations can be found in it.
We advise to use the target system of Xcode and have a distinct target for PROD and DEV.

This file should only be added to your DEV target.


#### Customize appearance

You can have fun with the supported configuration keys in the plist. The following keys will influence the UI rendering of the server list:

* **color\_alternate**				-> alternate text color, also the text color for the footer
* **color\_cell**					-> text color in the server cells
* **color\_cell\_separator**		-> separator color between server cells
* **color\_header\_bg**				-> background color for the header
* **color\_header\_separator**		-> separator color for the header
* **color\_main**					-> main text color, also the background color for the footer


The following keys will influence the behavior of the URL pinger:

* **host\_http\_header**				-> the HTTP method sent to the server to ping it
* **host\_status\_code\_expected**		-> the status code expected to be correct when pinging the server. set to 0 if any status code will do (default)
* **host\_timeout**						-> timeout before considering the URL is offline (seconds)


Finally, this key will influence how long the user will wait before automatically loading the app:

* **autoloading\_delay**


#### Add your environments to it

The server selector will list every servers that's specified in your plist file, in the /environments array.
Each entry in this array is expected to follow a specific structure :

* **display\_name** _[string]_ mandatory
* **display\_order** _[number]_ optional
* **is\_prod** _[bool]_ optional
* **host\_url** _[string]_ optional (if empty, it won't be pinged)
* **payload** _[dict]_ mandatory

The most important part here, is to fill in the payload dictionary with whatever configuration you wanna make available to your app, depending on the environment.
A good example would be: identifiers for Google Analytics, urls for webservices, etc.

### Step 4: test!

## More infos

### Environment variable support

Environment variables are variables that you can set at runtime during DEBUG. You can find it when editing a Xcode scheme, in Scheme > Run > Arguments > Environment variable.

This plugin currently supports only one flag.

#### Skipping server selector

If you don't wanna have the server selector show up all the time during debug because you're always using the same environment, you can actually skip the UI step and have it load the rest of the app right way.
To achieve this, set up the following key/values:

* **SKIP\_SERVER\_SELECTOR** => _<YOUR\_SERVER\_DISPLAY\_NAME>_