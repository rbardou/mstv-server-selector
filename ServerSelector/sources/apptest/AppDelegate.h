//
//  AppDelegate.h
//  ServerSelector
//
//  Created by Remy Bardou on 24/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAppConfig : NSObject

@property (nonatomic, copy) NSString*     envName;
@property (nonatomic, copy) NSDictionary* moma;
@property (nonatomic, copy) NSDictionary* cms;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) MyAppConfig *config;

@end

