//
//  ViewController.m
//  ServerSelector
//
//  Created by Remy Bardou on 24/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "ViewController.h"

#import "AppDelegate.h"

@interface ViewController ()

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *payloadLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.config) {
        self.titleLabel.text = [self.titleLabel.text stringByReplacingOccurrencesOfString:@"{ENV}" withString:[appDelegate.config.envName uppercaseString]];
        self.payloadLabel.text = [appDelegate.config.moma description];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
