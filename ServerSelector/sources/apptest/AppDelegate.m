//
//  AppDelegate.m
//  ServerSelector
//
//  Created by Remy Bardou on 24/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "AppDelegate.h"

//#define IS_PROD 0

/** Step 1: import the library header */
#import "MSTVServerSelector.h"

@implementation MyAppConfig
@end

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    /** Step 2: put your initial logic in a block and call it later */
    void(^myStartupLogic)() = ^() {
        // put your old logic here
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        UIViewController *vc = [sb instantiateInitialViewController];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        self.window.rootViewController = vc;
        [self.window makeKeyAndVisible];
    };

    
    [self loadServerSelectorIfNecessaryThenProceedWith:myStartupLogic];
    
#ifndef IS_PROD
    BOOL serverSelectorIsLoaded = NO;
    Class serverSelectorVCClass = NSClassFromString(@"MSTVServerSelectorViewController");
    if (serverSelectorVCClass) {
        serverSelectorIsLoaded = YES;
    }
    
    if (serverSelectorIsLoaded) {
        MSTVServerSelectorViewController *serverVC = (MSTVServerSelectorViewController *)self.window.rootViewController;
        
        if ([serverVC isKindOfClass:[MSTVServerSelectorViewController class]]) {
            [serverVC addActionOnServerSelected:^(NSString *envName, NSDictionary *payload, BOOL environmentHasChangedSinceLastLaunch) {
                
                // do stuff here
            }];
        }
    }
    
#endif
    
    return YES;
}

- (void) loadServerSelectorIfNecessaryThenProceedWith:(void (^)())proceedBlock {
    
    /** Step : for extra security, you can add a new USER_DEFINED variable that
        you set only for your ReleaseDistribution of prod (so AppStore basically)
     
        Check-out "Other C Flags" category in your target Build settings, then
        unfold it and add "-DIS_PROD" in the Release profile
     */
    
#ifndef IS_PROD
    BOOL loadServerSelector = NO;
    Class serverSelectorVCClass = NSClassFromString(@"MSTVServerSelectorViewController");
    if (serverSelectorVCClass) {
        loadServerSelector = YES;
    }
    
    if (loadServerSelector) {
        MSTVServerSelectorViewController *serverSelectorVC = [[serverSelectorVCClass alloc] initWithServerSelectionAction:^(NSString *envName, NSDictionary *payload, BOOL environmentHasChangedSinceLastLaunch) {
            
            MyAppConfig *config = [[MyAppConfig alloc] init];
            [payload enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
                [config setValue:obj forKey:key];
            }];
            self.config = config;
            
            if (environmentHasChangedSinceLastLaunch) {
                // Clean stuff
                NSLog(@"SELECTED ENVIRONMENT CHANGED SINCE LAST TIME");
                NSLog(@"CLEARING DOCUMENTS DIRECTORY");
                
                NSString *folderPath = nil;
                NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                if ([paths count] > 0) folderPath = [paths objectAtIndex:0];
                
                NSError *error = nil;
                
                NSArray *documentsDirContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error];
                for (NSString *file in documentsDirContent) {
                    [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
                    if (error) {
                        NSLog(@"ERROR deleting %@ (%@)", file, error);
                    }
                }
            }
            
            proceedBlock();
        }];
        
        self.window.rootViewController = serverSelectorVC;
        [self.window makeKeyAndVisible];
    } else {
#endif
        proceedBlock();
#ifndef IS_PROD
    }
#endif
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
