//
//  MSTVServerSelectorTitleView.h
//  ServerSelector
//
//  Created by Remy Bardou on 19/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MSTVServerSelectorConfig;

@interface MSTVServerSelectorTitleView : UIView

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *subtitleLabel;

+ (instancetype) newTitleView;

- (void)applyStyleFromConfig:(MSTVServerSelectorConfig *)config;

@end
