//
//  MSTVServerSelectorViewController.h
//  ingdirect
//
//  Created by Remy Bardou on 13/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^MSTVServerSelectionAction)(NSString *envName, NSDictionary *payload, BOOL environmentHasChangedSinceLastLaunch);

@interface MSTVServerSelectorListController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

- (void) addActionOnServerSelected:(MSTVServerSelectionAction)onServerSelected;
- (void) removeAllOtherActionsOnServerSelected;

@end
