//
//  MSTVServerSelectorViewController.m
//  ingdirect
//
//  Created by Remy Bardou on 13/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "MSTVServerSelectorListController.h"
#import "MSTVServerSelectorListController_Private.h"
#import "MSTVServerSelectorCellTableViewCell.h"
#import "MSTVServerSelectorLoadingViewController.h"
#import "MSTVServerSelectorTitleView.h"
#import "MSTVServerSelectorViewController.h"

#import "MSTVServerSelectorConfig.h"
#import "MSTVServerSelectorEnvironment.h"

CFTimeInterval const animationDuration = 0.2f;

@interface MSTVServerSelectorListController () <MSTVServerSelectorLoadingDelegate>

// UI
@property (nonatomic, readonly) MSTVServerSelectorTitleView *customTitleView;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *tableViewBottomMargin;

@property (nonatomic, strong) IBOutlet UIView *footerView;
@property (nonatomic, strong) IBOutlet UILabel *footerTitleLabel;
@property (nonatomic, strong) IBOutlet UISwitch *rememberMeSwitch;
@property (nonatomic, strong) IBOutlet UILabel *rememberMeCaptionLabel;
@property (nonatomic, strong) IBOutlet UISwitch *noCancelSwitch;
@property (nonatomic, strong) IBOutlet UILabel *noCancelLabel;
@property (nonatomic, strong) IBOutlet UIButton *footerButton;

@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *styleableViews;

// data
@property (nonatomic, strong) UIImage *cellTintedImageCached;
@property (nonatomic, strong) MSTVServerSelectorConfig *config;

@end

@interface MSTVServerSelectorListController ()

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation MSTVServerSelectorListController

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (self.config.noCancelEnabled && self.config.shouldAutoloadEnvironment) {
		[self loadLastKnownEnv];
		return;
	}
	
    // setup pull-to-refresh on the server list
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self.tableView sendSubviewToBack:self.refreshControl];
    
    [self.tableView registerNib:[MSTVServerSelectorCellTableViewCell defaultNib] forCellReuseIdentifier:[MSTVServerSelectorCellTableViewCell defaultIdentifier]];
    
#if DEBUG
    NSString *skipAndLoadEnv = [NSProcessInfo processInfo].environment[@"SKIP_SERVER_SELECTOR"];
    if (skipAndLoadEnv.length > 0) {
        MSTVServerSelectorEnvironment *env = [self.config environmentWithName:skipAndLoadEnv];
        if (env) {
            fprintf(stderr, "\n***************************\n");
            fprintf(stderr, "SKIPPING SERVER SELECTOR\n");
            fprintf(stderr, "Loading environment: %s\n", skipAndLoadEnv.UTF8String);
            fprintf(stderr, "***************************\n");
            
            [self loadEnv:env];
            return;
        }
    }
#endif
    
    self.navigationItem.titleView = [MSTVServerSelectorTitleView newTitleView];
    
    self.rememberMeCaptionLabel.text = NSLocalizedStringFromTable(@"ServerSelectorAutoloadCaption", kMSTVServerSelectorStringsFile, nil);
	self.noCancelLabel.text = NSLocalizedStringFromTable(@"ServerSelectorNoCancelCaption", kMSTVServerSelectorStringsFile, nil);
    self.customTitleView.titleLabel.text = NSLocalizedStringFromTable(@"ServerSelectorHeaderTitle", kMSTVServerSelectorStringsFile, nil);
    self.customTitleView.subtitleLabel.text = [self appVersionDisplay];
    
    [self applyStyleFromConfig];
    [self loadPanels];
}

- (void) addActionOnServerSelected:(MSTVServerSelectionAction)onServerSelected {
    if (onServerSelected)
        [self.additionalCallbacksOnServerSelected addObject:[onServerSelected copy]];
}

- (void) removeAllOtherActionsOnServerSelected {
    [self.additionalCallbacksOnServerSelected removeAllObjects];
}

- (NSMutableArray *) additionalCallbacksOnServerSelected {
    if (!_additionalCallbacksOnServerSelected) {
        _additionalCallbacksOnServerSelected = [[NSMutableArray alloc] init];
    }
    return _additionalCallbacksOnServerSelected;
}

- (void) refreshData {
	
	// Reset all environment's last known status to unknown
	[self.config.environments enumerateObjectsUsingBlock:^(MSTVServerSelectorEnvironment *env, NSUInteger idx, BOOL * _Nonnull stop) {
		env.lastKnownStatus = MSTVServerSelectorEnvironmentReachabilityStatus_Unknown;
	}];
	
	
    // and tell every visible cell to retry a server connection
	// to the server and check the reachability status
    [[self.tableView visibleCells] enumerateObjectsUsingBlock:^(MSTVServerSelectorCellTableViewCell *cell, NSUInteger idx, BOOL *stop) {
        [cell refreshOnlineStatus];
    }];
    [self.refreshControl endRefreshing];
}

- (NSString *) appVersionDisplay {
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSString *displayVersion = [NSString stringWithFormat:@"v%@ [%@]", version, build];
    return displayVersion;
}

- (void) loadPanels {
    if (self.config.shouldAutoloadEnvironment) {
        MSTVServerSelectorLoadingViewController *vc = [[MSTVServerSelectorLoadingViewController alloc] initWithConfig:self.config delegate:self];
        [self.navigationController presentViewController:vc animated:NO completion:nil];
    }
}

- (MSTVServerSelectorConfig *) config {
    if (!_config) {
        _config = [[MSTVServerSelectorConfig alloc] init];
    }
    return _config;
}

- (MSTVServerSelectorTitleView *)customTitleView {
    return (MSTVServerSelectorTitleView *)self.navigationItem.titleView;
}

#pragma mark - Styling stuff

- (void) applyStyleFromConfig {
    MSTVServerSelectorConfig *config = self.config;
    [self.styleableViews enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL *stop) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)view;
            
            if (config.altColor)
                button.backgroundColor = config.altColor;
            
            if (config.mainColor)
                [button setTitleColor:config.mainColor forState:UIControlStateNormal];
        } else if ([view isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel *)view;
            
            if (config.altColor)
                label.textColor = config.altColor;
        } else if ([view isKindOfClass:[UIProgressView class]]) {
            UIProgressView *progressView = (UIProgressView *)view;
            
            if (config.mainColor)
                progressView.progressTintColor = config.mainColor;
            
            if (config.altColor)
                progressView.trackTintColor = config.altColor;
        } else {
            if (config.mainColor)
                view.backgroundColor = config.mainColor;
        }
    }];
    
    [((MSTVServerSelectorTitleView *)self.navigationItem.titleView) applyStyleFromConfig:config];
}

- (UIImage *) cellTintedImageFromImage:(UIImage *)image {
    if (!_cellTintedImageCached) {
        _cellTintedImageCached = [self.class tintImage:image withColor:self.config.mainColor];
    }
    return _cellTintedImageCached;
}

/** Code is put here to not rely on any external sources */
+ (UIImage *) tintImage:(UIImage *)sourceImage withColor:(UIColor *)tintColor {
    CGFloat scale = sourceImage.scale;
    CGSize size = sourceImage.size;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 size.width * scale,
                                                 size.height * scale,
                                                 8, 0, colorSpace,
                                                 (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    CGContextScaleCTM(context, scale, scale);
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    // Draw alpha-mask
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, sourceImage.CGImage);
    
    // Draw tint color, preserving alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextSetFillColorWithColor(context, tintColor.CGColor);
    CGContextFillRect(context, rect);
    
    // Create UIImage
    CGImageRef tintedImageRef = CGBitmapContextCreateImage(context);
    UIImage *tintedImage = [UIImage imageWithCGImage:tintedImageRef
                                               scale:scale
                                         orientation:sourceImage.imageOrientation];
    
    CGImageRelease(tintedImageRef);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    return tintedImage;
}

#pragma mark - Environment interactions

- (void) loadLastKnownEnv {
    MSTVServerSelectorEnvironment *environment = self.config.environmentToAutoload;
    [self loadEnv:environment];
}

- (void) loadEnv:(MSTVServerSelectorEnvironment *)environment {
    if (environment && self.onServerSelected) {
        self.onServerSelected(environment.uniqueName, environment.payload, ![self.config environmentIsLastLoadedEnvironment:environment]);
        
        __weak typeof(&*self) weakSelf = self;
        [self.additionalCallbacksOnServerSelected enumerateObjectsUsingBlock:^(MSTVServerSelectionAction obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj(environment.uniqueName, environment.payload, ![weakSelf.config environmentIsLastLoadedEnvironment:environment]);
        }];
        
        [self.config saveLastLoadedEnvironment:environment];
    }
}

#pragma mark - Actions

- (IBAction) didChangeToggleValue:(UISwitch *)sender {
    if (!sender.on) {
        [self.config forgetAutoloadingEnvironment];
    }
	
	self.noCancelSwitch.enabled = sender.on;
	self.noCancelSwitch.on = NO;
}

#pragma mark - TableViewDelegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.config.environments.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MSTVServerSelectorCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[MSTVServerSelectorCellTableViewCell defaultIdentifier] forIndexPath:indexPath];
    
    MSTVServerSelectorEnvironment *env = self.config.environments[indexPath.row];
    [cell setupWithEnvironment:env];
    cell.serverIcon.image = [self cellTintedImageFromImage:cell.serverIcon.image];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *configuration = self.config.environments[indexPath.row];
    return [MSTVServerSelectorCellTableViewCell heightForCellWithData:configuration];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self showFooter:self.footerView forEnvironment:self.config.environments[indexPath.row] onComplete:^{
        [self.tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionNone animated:YES];
    }];
}

#pragma mark - UIScrollViewDelegate

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self hideFooter:self.footerView];
}

#pragma mark - MSTVServerSelectorLoadingDelegate

- (void) timerElapsed {
#if DEBUG
    if (![[NSProcessInfo processInfo].arguments containsObject:@"DISABLE_TIMER_EXPIRATION"]) {
        [self loadLastKnownEnv];
    }
#else
    [self loadLastKnownEnv];
#endif
}

- (void) timerCanceled {
    [self.config forgetAutoloadingEnvironment];
}

#pragma mark - Footer interactions

- (void) hideFooter:(UIView *)footer {
    [self hideFooter:footer onCompletion:nil];
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

- (void) hideFooter:(UIView *)footer onCompletion:(void(^)())onCompletion {
    if (footer.superview) {
        self.tableViewBottomMargin.constant = 0;
        
        UIView *container = self.view;
        [UIView animateWithDuration:animationDuration animations:^{
            footer.frame = footer.frame = CGRectMake(0,
                                                   container.frame.size.height,
                                                   container.frame.size.width,
                                                   footer.frame.size.height);
        } completion:^(BOOL finished) {
            if (onCompletion)
                onCompletion();
        }];
    } else {
        if (onCompletion)
            onCompletion();
    }
}

- (void) showFooter:(UIView *)footer
     forEnvironment:(MSTVServerSelectorEnvironment *)environment onComplete:(void(^)())onComplete {
    NSString * const displayName = [environment.displayName uppercaseString];
    
    self.footerTitleLabel.text = [NSString stringWithFormat:NSLocalizedStringFromTable(@"ServerSelectorDetailsTitle", kMSTVServerSelectorStringsFile, nil), displayName];
    
    [self.footerButton setTitle:[NSString stringWithFormat:NSLocalizedStringFromTable(@"ServerSelectorDetailsButton", kMSTVServerSelectorStringsFile, nil), displayName] forState:UIControlStateNormal];
    
    UIView *container = self.view;
        CGFloat containerHeight = container.frame.size.height;
        CGFloat containerWidth  = container.frame.size.width;
    
    BOOL shouldExchangeHeightAndWidth = NO;
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        // WIDTH should be greater than height
        if (containerHeight > containerWidth)
            shouldExchangeHeightAndWidth = YES;
    } else {
        // HEIGHT should be greater than width
        if (containerHeight < containerWidth)
            shouldExchangeHeightAndWidth = YES;
    }
    
    if (shouldExchangeHeightAndWidth) {
        CGFloat pivot = containerHeight;
        containerHeight = containerWidth;
        containerWidth = pivot;
    }
    
    if (!footer.superview) {
        footer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        
        footer.frame = CGRectMake(0,
                               containerHeight,
                               containerWidth,
                               footer.frame.size.height);
        [container addSubview:footer];
    }
    
    self.tableViewBottomMargin.constant = footer.frame.size.height;
    [UIView animateWithDuration:animationDuration animations:^{
        footer.frame = CGRectMake(footer.frame.origin.x,
                                  containerHeight - footer.frame.size.height,
                                  containerWidth,
                                  footer.frame.size.height);
    } completion:^(BOOL finished) {
        if (onComplete) onComplete();
    }];
}

- (IBAction)didTapSelectEnvironment:(UIButton *)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if (self.onServerSelected) {
        MSTVServerSelectorEnvironment *environment = self.config.environments[indexPath.row];
        self.onServerSelected(environment.uniqueName, environment.payload, ![self.config environmentIsLastLoadedEnvironment:environment]);
        
        __weak typeof(&*self) weakSelf = self;
        [self.additionalCallbacksOnServerSelected enumerateObjectsUsingBlock:^(MSTVServerSelectionAction obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj(environment.uniqueName, environment.payload, ![weakSelf.config environmentIsLastLoadedEnvironment:environment]);
        }];
        
        [self.config saveLastLoadedEnvironment:environment];
        
        if (self.rememberMeSwitch.on) {
            [self.config saveAutoloadingEnvironment:environment];
			
			if (self.noCancelSwitch.on) {
				[self.config enableNoCancel];
			}
        }
    }
}

@end
