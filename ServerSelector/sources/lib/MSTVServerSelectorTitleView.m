//
//  MSTVServerSelectorTitleView.m
//  ServerSelector
//
//  Created by Remy Bardou on 19/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "MSTVServerSelectorTitleView.h"
#import "MSTVServerSelectorConfig.h"

@implementation MSTVServerSelectorTitleView

+ (instancetype) newTitleView {
    MSTVServerSelectorTitleView *v = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:nil options:nil] firstObject];
    return v;
}

- (void) applyStyleFromConfig:(MSTVServerSelectorConfig *)config {
    self.backgroundColor = config.headerColor;
    
    self.titleLabel.textColor = config.mainColor;
    self.titleLabel.backgroundColor = self.backgroundColor;
    
    self.subtitleLabel.textColor = config.headerSeparatorColor;
}

@end
