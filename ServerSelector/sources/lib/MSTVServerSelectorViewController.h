//
//  MSTVServerSelectorNavController.h
//  ServerSelector
//
//  Created by Remy Bardou on 19/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSTVServerSelectorListController.h"

extern NSString * const kMSTVServerSelectorStringsFile;

@interface MSTVServerSelectorViewController : UINavigationController

- (id) initWithServerSelectionAction:(MSTVServerSelectionAction)onServerSelected;
- (void) addActionOnServerSelected:(MSTVServerSelectionAction)onServerSelected;
- (void) removeAllOtherActionsOnServerSelected;

@end
