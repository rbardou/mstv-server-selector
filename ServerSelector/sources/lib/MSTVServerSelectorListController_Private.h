//
//  MSTVServerSelectorListController_Private.h
//  ServerSelector
//
//  Created by Remy Bardou on 19/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "MSTVServerSelectorListController.h"

@interface MSTVServerSelectorListController ()

// config
@property (nonatomic, copy) MSTVServerSelectionAction onServerSelected;
@property (nonatomic, strong) NSMutableArray *additionalCallbacksOnServerSelected;

@end