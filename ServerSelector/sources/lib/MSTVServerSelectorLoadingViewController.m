//
//  MSTVServerSelectorLoadingViewController.m
//  ServerSelector
//
//  Created by Remy Bardou on 19/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "MSTVServerSelectorLoadingViewController.h"
#import "MSTVServerSelectorViewController.h"

#import "MSTVServerSelectorConfig.h"
#import "MSTVServerSelectorEnvironment.h"

@interface MSTVServerSelectorLoadingViewController ()

// UI
@property (nonatomic, weak) IBOutlet UILabel *loadingLabel;
@property (nonatomic, weak) IBOutlet UILabel *versionLabel;

@property (nonatomic, weak) IBOutlet UIView *bottomView;
@property (nonatomic, weak) IBOutlet UIProgressView *progressView;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIButton *skipButton;

// Misc
@property (nonatomic, strong) NSTimer *autoLoadingTimer;
@property (nonatomic, strong) NSDate  *timerStartDate;

@end

@implementation MSTVServerSelectorLoadingViewController

- (id) initWithConfig:(MSTVServerSelectorConfig *)config
             delegate:(id<MSTVServerSelectorLoadingDelegate>)delegate {
    self = [self initWithNibName:NSStringFromClass(self.class) bundle:[NSBundle mainBundle]];
    if (self) {
        self.config = config;
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.loadingLabel.text = [NSString stringWithFormat:NSLocalizedStringFromTable(@"ServerSelectorHeaderTitleLoading", kMSTVServerSelectorStringsFile, nil), self.config.environmentKeyToAutoload];
    self.versionLabel.text = [self appVersionDisplay];
    
    [self.cancelButton setTitle:NSLocalizedStringFromTable(@"ServerSelectorCancelButton", kMSTVServerSelectorStringsFile, nil) forState:UIControlStateNormal];
    [self.skipButton setTitle:NSLocalizedStringFromTable(@"ServerSelectorSkipButton", kMSTVServerSelectorStringsFile, nil) forState:UIControlStateNormal];
    
    [self applyStyleFromConfig:self.config];
    
    [self startTimer];
}

- (NSString *) appVersionDisplay {
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSString *displayVersion = [NSString stringWithFormat:@"v%@ [%@]", version, build];
    return displayVersion;
}

- (void) dealloc {
    [self stopTimer];
}

- (void) applyStyleFromConfig:(MSTVServerSelectorConfig *)config {
    if (config.altColor) {
        self.cancelButton.backgroundColor = config.altColor;
        self.skipButton.backgroundColor = config.altColor;
        
        self.progressView.trackTintColor = config.altColor;
        
        self.view.backgroundColor = config.altColor;
    }
    
    if (config.mainColor) {
        [self.cancelButton setTitleColor:config.mainColor forState:UIControlStateNormal];
        [self.skipButton setTitleColor:config.mainColor forState:UIControlStateNormal];
        
        self.progressView.progressTintColor = config.mainColor;
        self.bottomView.backgroundColor = config.mainColor;
        
        self.loadingLabel.textColor = config.mainColor;
    }
}

#pragma mark - Timer stuff

- (void) startTimer {
        // start auto update timer
        [self.autoLoadingTimer invalidate];
        self.autoLoadingTimer = [NSTimer scheduledTimerWithTimeInterval:kServerSelectorProgressBarTimerRate target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
        self.timerStartDate = [[NSDate alloc] init];
}

- (void) stopTimer {
    [self.autoLoadingTimer invalidate];
    self.autoLoadingTimer = nil;
}

- (void) updateTimer:(NSTimer *)timer {
    if (self.progressView.progress >= 1.0f) {
        [self.autoLoadingTimer invalidate];
        self.autoLoadingTimer = nil;
        
        if ([self.delegate respondsToSelector:@selector(timerElapsed)])
            [self.delegate timerElapsed];
    } else {
        // instead of incrementing by the same amount every tick, calculate the
        // absolute value of the progress based on the supposed total duration
        // this is a better solution, since it is framerate independent
        
        // how long since the timer started?
        NSTimeInterval timeElapsed = -[self.timerStartDate timeIntervalSinceNow];
        self.progressView.progress = timeElapsed / self.config.autoloadingDelay;
    }
}

#pragma mark - Actions

- (IBAction)didTapCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(timerCanceled)]) {
            [self.delegate timerCanceled];
        }
    }];
}

- (IBAction)didTapSkipButton:(id)sender {
    [self stopTimer];
    
    if ([self.delegate respondsToSelector:@selector(timerElapsed)]) {
        [self.delegate timerElapsed];
    }
}

@end
