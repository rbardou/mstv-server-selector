//
//  MSTVServerSelectorCellTableViewCell.h
//  ingdirect
//
//  Created by Remy Bardou on 16/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MSTVServerSelectorConfig;
@class MSTVServerSelectorEnvironment;

@interface MSTVServerSelectorCellTableViewCell : UITableViewCell

+ (NSString *) defaultIdentifier;
+ (UINib *) defaultNib;
+ (CGFloat) heightForCellWithData:(NSDictionary *)cellData;

@property (nonatomic, strong) IBOutlet UILabel *serverNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *serverIcon;

@property (nonatomic, strong) IBOutlet UIView *reachabilityView;
@property (nonatomic, strong) IBOutlet UILabel *reachabilityLabel;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *reachabilityActivityIndicator;

@property (nonatomic, strong) IBOutlet UIView *separator;

- (void) setupWithEnvironment:(MSTVServerSelectorEnvironment *)data;
- (void) refreshOnlineStatus;

@end
