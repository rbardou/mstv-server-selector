//
//  MSTVServerSelectorEnvironment.h
//  ingdirect
//
//  Created by Remy Bardou on 17/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MSTVServerSelectorConfig;

typedef enum : NSInteger {
    MSTVServerSelectorEnvironmentReachabilityStatus_Unknown,
    MSTVServerSelectorEnvironmentReachabilityStatus_Reachable,
    MSTVServerSelectorEnvironmentReachabilityStatus_Unreachable
} MSTVServerSelectorEnvironmentReachabilityStatus;

@interface MSTVServerSelectorEnvironment : NSObject

@property (nonatomic, readonly) NSString *uniqueName;
@property (nonatomic, readonly) NSString *displayName;
@property (nonatomic, readonly) NSInteger displayOrder;
@property (nonatomic, readonly) NSString *hostUrl;
@property (nonatomic, readonly) BOOL isProd;
@property (nonatomic, readonly) NSDictionary *payload;

@property (nonatomic, readonly) MSTVServerSelectorConfig *config;

@property (nonatomic, assign) MSTVServerSelectorEnvironmentReachabilityStatus lastKnownStatus;

- (id) initWithDictionary:(NSDictionary *)dictionary config:(MSTVServerSelectorConfig *)config;

@end
