//
//  MSTVServerSelectorNavController.m
//  ServerSelector
//
//  Created by Remy Bardou on 19/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "MSTVServerSelectorViewController.h"
#import "MSTVServerSelectorListController_Private.h"

NSString * const kMSTVServerSelectorStringsFile = @"MSTVServerSelector";

@interface MSTVServerSelectorViewController ()

@end

@implementation MSTVServerSelectorViewController

- (id) initWithServerSelectionAction:(MSTVServerSelectionAction)onServerSelected {
    MSTVServerSelectorListController *vc = [[MSTVServerSelectorListController alloc] init];
    vc.onServerSelected = onServerSelected;
    
    self = [self initWithRootViewController:vc];
    if (self) {
        
    }
    
    return self;
}

- (void) addActionOnServerSelected:(MSTVServerSelectionAction)onServerSelected {
    MSTVServerSelectorListController *listController = (MSTVServerSelectorListController *)[self.viewControllers firstObject];
    if ([listController isKindOfClass:[MSTVServerSelectorListController class]]) {
        [listController addActionOnServerSelected:onServerSelected];
    }
}

- (void) removeAllOtherActionsOnServerSelected {
    MSTVServerSelectorListController *listController = (MSTVServerSelectorListController *)[self.viewControllers firstObject];
    if ([listController isKindOfClass:[MSTVServerSelectorListController class]]) {
        [listController removeAllOtherActionsOnServerSelected];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
