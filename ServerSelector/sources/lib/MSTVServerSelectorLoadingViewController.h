//
//  MSTVServerSelectorLoadingViewController.h
//  ServerSelector
//
//  Created by Remy Bardou on 19/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MSTVServerSelectorLoadingDelegate <NSObject>

- (void)timerElapsed;
- (void)timerCanceled;

@end

@class MSTVServerSelectorConfig;
@class MSTVServerSelectorEnvironment;

@interface MSTVServerSelectorLoadingViewController : UIViewController

@property (nonatomic, strong) MSTVServerSelectorConfig *config;
@property (nonatomic, weak) id<MSTVServerSelectorLoadingDelegate>delegate;

- (id) initWithConfig:(MSTVServerSelectorConfig *)config
             delegate:(id<MSTVServerSelectorLoadingDelegate>)delegate;

@end
