//
//  MSTVServerSelector.h
//  ServerSelector
//
//  Created by Remy Bardou on 24/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

/** This is the only header that should be needed to import in the 
    external project.
 */
#import "MSTVServerSelectorViewController.h"
