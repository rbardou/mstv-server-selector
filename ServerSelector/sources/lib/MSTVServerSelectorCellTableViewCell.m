//
//  MSTVServerSelectorCellTableViewCell.m
//  ingdirect
//
//  Created by Remy Bardou on 16/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//


#import "MSTVServerSelectorCellTableViewCell.h"
#import "MSTVServerSelectorViewController.h"
#import "MSTVServerSelectorConfig.h"
#import "MSTVServerSelectorEnvironment.h"

@interface MSTVServerSelectorCellTableViewCell ()<NSURLConnectionDelegate>

@property (nonatomic, strong) UIColor *reachableColor;
@property (nonatomic, strong) UIColor *unreachableColor;
@property (nonatomic, strong) UIColor *unknownReachabilityColor;

@property (nonatomic, strong) UIFont *baseFont;
@property (nonatomic, strong) UIFont *boldFont;

@property (nonatomic, strong) NSURLConnection *connection;

@property (nonatomic, weak) MSTVServerSelectorEnvironment *environment;

@end

@implementation MSTVServerSelectorCellTableViewCell

#pragma mark - Lifecycle

+ (UINib *) defaultNib {
    return [UINib nibWithNibName:NSStringFromClass(self.class) bundle:[NSBundle mainBundle]];
}

+ (NSString *) defaultIdentifier {
    return @"serverCell";
}

+ (CGFloat) heightForCellWithData:(NSDictionary *)cellData {
    return 66.0f;
}

- (void)awakeFromNib {
	[super awakeFromNib];
	
    // Initialization code
    self.reachabilityView.layer.shouldRasterize = YES;
    self.reachabilityView.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (void) dealloc {
    [self stopPingingHost];
}

- (void) prepareForReuse {
    [self.reachabilityLabel.layer removeAllAnimations];
    [self.reachabilityActivityIndicator.layer removeAllAnimations];
    
    [self markAsUnknown];
    [self stopPingingHost];
}

- (UIFontDescriptor *) legacyFontDescriptorWithFaceAttribute:(NSString *)faceAttribute
													 forFont:(UIFont *)font {
	if (!faceAttribute) return nil;
	NSString *fontFamily = font.familyName;
	if (!fontFamily) return nil;
	
	CGFloat pointSize = font.pointSize;
	NSDictionary *fontAttributes = @{UIFontDescriptorFamilyAttribute: fontFamily,
									 UIFontDescriptorFaceAttribute: faceAttribute,
									 UIFontDescriptorSizeAttribute: [NSString stringWithFormat:@"%.1f", pointSize]};
	UIFontDescriptor *descriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:fontAttributes];
	return descriptor;
}

- (UIFontDescriptor *) boldFontDescriptorForFont:(UIFont *)font {
	// iOS9+ handles this correctly
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.99f) {
		return [[font fontDescriptor] fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
	}
	return [self legacyFontDescriptorWithFaceAttribute:@"Bold" forFont:font];
}

- (UIFont *)boldFont {
    if (!_boldFont) {
        UIFont *font = self.baseFont;
		
		_boldFont = [UIFont fontWithDescriptor:[self boldFontDescriptorForFont:font] size:font.pointSize];
    }
    return _boldFont;
}

- (UIFont *)baseFont {
    if (!_baseFont) {
        _baseFont = self.serverNameLabel.font;
    }
    return _baseFont;
}

#pragma mark - Data handling

- (void) setupWithEnvironment:(MSTVServerSelectorEnvironment *)data {
    
    self.environment = data;
    
    [self evaluateReachabilityStatusAnimated:NO];
    
    if (data.isProd) {
        
        self.serverNameLabel.font = self.boldFont;
        self.serverNameLabel.textColor = self.environment.config.cellProductionColor;
        self.serverNameLabel.text = NSLocalizedStringFromTable(@"ServerSelectorCellProductionLabel", kMSTVServerSelectorStringsFile, nil);
    } else {
        self.serverNameLabel.font = self.baseFont;
        self.serverNameLabel.textColor = self.environment.config.cellColor;
        self.serverNameLabel.text = [data.displayName uppercaseString];
    }
    
    self.separator.backgroundColor = self.environment.config.cellSeparatorColor;
    
    [self refreshOnlineStatusAfterStatusReset:NO];
}

- (void) evaluateReachabilityStatusAnimated:(BOOL)animated {
    MSTVServerSelectorEnvironmentReachabilityStatus status = self.environment.lastKnownStatus;
    switch (status) {
        case MSTVServerSelectorEnvironmentReachabilityStatus_Reachable:
            self.reachabilityView.layer.borderColor = self.reachableColor.CGColor;
            self.reachabilityLabel.text = NSLocalizedStringFromTable(@"ServerSelectorCellStatusOnline", kMSTVServerSelectorStringsFile, nil);
            self.reachabilityLabel.textColor = self.reachableColor;
            [self reachabilityVisible:YES animated:animated];
            break;
        case MSTVServerSelectorEnvironmentReachabilityStatus_Unreachable:
            self.reachabilityView.layer.borderColor = self.unreachableColor.CGColor;
            self.reachabilityLabel.text = NSLocalizedStringFromTable(@"ServerSelectorCellStatusOffline", kMSTVServerSelectorStringsFile, nil);
            self.reachabilityLabel.textColor = self.unreachableColor;
            [self reachabilityVisible:YES animated:animated];
            break;
        default:
            [self markAsUnknown];
            break;
    }
}

- (void) reachabilityVisible:(BOOL)visible animated:(BOOL)animated {
    [UIView animateWithDuration:(animated ? 0.5f : 0.0f) animations:^{
        self.reachabilityActivityIndicator.alpha = visible ? 0.0f : 1.0f;
        self.reachabilityLabel.alpha = visible ? 1.0f : 0.0f;
    }];
}

- (void) markAsReachable {
    [self.reachabilityActivityIndicator stopAnimating];
    self.environment.lastKnownStatus = MSTVServerSelectorEnvironmentReachabilityStatus_Reachable;
    [self evaluateReachabilityStatusAnimated:YES];
}

- (void) markAsUnreachable {
    [self.reachabilityActivityIndicator stopAnimating];
    self.environment.lastKnownStatus = MSTVServerSelectorEnvironmentReachabilityStatus_Unreachable;
    [self evaluateReachabilityStatusAnimated:YES];
}

- (void) markAsUnknown {
    [self.reachabilityActivityIndicator startAnimating];
    self.reachabilityView.layer.borderColor = self.unknownReachabilityColor.CGColor;
    self.reachabilityLabel.text = NSLocalizedStringFromTable(@"ServerSelectorCellStatusOnline", kMSTVServerSelectorStringsFile, nil);
    self.reachabilityLabel.textColor = self.unknownReachabilityColor;
    [self reachabilityVisible:NO animated:NO];
}

- (void) refreshOnlineStatus {
    [self refreshOnlineStatusAfterStatusReset:YES];
}

- (void) refreshOnlineStatusAfterStatusReset:(BOOL)performStatusReset {
    
    if (performStatusReset) {
        self.environment.lastKnownStatus = MSTVServerSelectorEnvironmentReachabilityStatus_Unknown;
        [self prepareForReuse];
        [self evaluateReachabilityStatusAnimated:YES];
    }
    
    NSString *serverHost = self.environment.hostUrl;
    if (serverHost.length > 0) {
        self.reachabilityView.hidden = NO;
        [self startPingingHost:serverHost];
    } else {
        self.reachabilityView.hidden = YES;
    }
}

#pragma mark - Reachability stuff

- (void) startPingingHost:(NSString *)host {
    if (self.connection) {
        NSLog(@"Connection already started");
        return;
    }
    
    NSURL *hostURL = [NSURL URLWithString:host];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:hostURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:self.environment.config.hostTimeout];
    [request setHTTPMethod:self.environment.config.hostHttpHeader];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
}

- (void) stopPingingHost {
    [self.connection cancel];
    self.connection = nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    if (self.environment.config.hostStatusCodeExpected == 0) {
        [self markAsReachable];
    } else {
        NSInteger statusCodeObtained = [(NSHTTPURLResponse *)response statusCode];
        if (statusCodeObtained == self.environment.config.hostStatusCodeExpected) {
            [self markAsReachable];
        } else {
            [self markAsUnreachable];
        }
    }
    [self markAsReachable];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self markAsUnreachable];
}

-(void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        NSURL* baseURL = connection.currentRequest.URL;
        if ([challenge.protectionSpace.host isEqualToString:baseURL.host]) {
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        } else {
            NSLog(@"Not trusting connection to host %@", challenge.protectionSpace.host);
        }
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

@end
