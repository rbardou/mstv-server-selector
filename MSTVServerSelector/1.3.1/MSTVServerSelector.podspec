Pod::Spec.new do |s|
  s.name         = "MSTVServerSelector"
  s.version      = "1.3.1"
  s.summary      = "MSTVServerSelector."
  s.homepage     = "http://www.mystudiofactory.com"
  s.license = {
      :type => 'Commercial',
      :text => <<-LICENSE
  		Copyright 2009 - 2016 Mystudiofactory.com. All rights reserved.
  	  	LICENSE
  }
  s.author             = { "Remy Bardou" => "rbardou@mystudiofactory.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://bitbucket.org/rbardou/mstv-server-selector.git", :tag => "v1.3.1" }
  s.source_files  = "ServerSelector/sources/lib/*.{h,m}"
  s.exclude_files = "Classes/Exclude"
  s.resources = "ServerSelector/resources/lib/*.{xcassets,xib,strings}"
  s.requires_arc = true
end
